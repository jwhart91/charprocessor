package charprocessor;

public class UpperCaseBlock implements Block {
    public String eval(char input) {
        return String.valueOf(Character.toUpperCase(input));
    }
}
