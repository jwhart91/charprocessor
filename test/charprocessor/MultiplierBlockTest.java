package charprocessor;

import org.junit.runners.Parameterized.*;
import java.util.Arrays;

public class MultiplierBlockTest extends BlockTest {
    public MultiplierBlockTest(char input, String expected) {
        super(input, expected);
    }

    @Parameters
    public static Iterable<Object[]> parameterSetup() {
        return Arrays
                .asList(new Object[][] {{ 'a', "aa" },
                                        { 'Z', "ZZ" },
                                        { 'b', "bb" },
                                        { '8', "88" },
                                        { '&', "&&" } });
    }

    public Block createBlock() {
        return new MultiplierBlock();
    }
}
