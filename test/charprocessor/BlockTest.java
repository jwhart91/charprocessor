package charprocessor;

import org.junit.runners.*;
import org.junit.runner.*;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

@RunWith(Parameterized.class)
public abstract class BlockTest {
    private Block testBlock;

    public abstract Block createBlock();

    @Before
    public void setUp() {
        testBlock = createBlock();
    }

    private char testInput;
    private String testExpected;

    public BlockTest(char input, String expected) {
        testInput = input;
        testExpected = expected;
    }

    @Test
    public void testConverter() {
        assertEquals(testExpected, testBlock.eval(testInput));
    }
}
