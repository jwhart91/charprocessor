package charprocessor;

import static org.junit.Assert.*;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

public class BlockLoaderTest {
    @Test
    public void testLoadBlockWithClassWithoutArgs()
            throws InstantiationException, IllegalAccessException, ClassNotFoundException, NoSuchMethodException, SecurityException, IllegalArgumentException, InvocationTargetException {
        BlockLoader blockLoader = new BlockLoader();
        Block upperCaseBlock = blockLoader.loadBlock("charprocessor.UpperCaseBlock");
        assertTrue(upperCaseBlock instanceof UpperCaseBlock);
    }

    @Test
    public void testLoadBlockWithClassWithArgs()
            throws InstantiationException, IllegalAccessException, ClassNotFoundException, NoSuchMethodException, SecurityException, IllegalArgumentException, InvocationTargetException {
        BlockLoader blockLoader = new BlockLoader();
        Block testBlock = blockLoader.loadBlock("charprocessor.CharRemoverBlock Z");
        assertTrue(testBlock instanceof CharRemoverBlock);
    }

    @Test
    public void testLoadBlockWithBadBlockName() throws NoSuchMethodException, SecurityException, IllegalArgumentException, InvocationTargetException {
        try {
            BlockLoader blockLoader = new BlockLoader();
            @SuppressWarnings("unused")
            Block testBlock = blockLoader.loadBlock("charprocessor.BadClass");
            fail("Should have raised ClassNotFoundException");
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
            assertTrue(true);
        }
    }

    @Test
    public void testGetBlocksFromNameWithLegalBlockNames(){
        List<String> blockNames = new ArrayList<>(Arrays.asList(
                "charprocessor.LowerCaseBlock",
                "charprocessor.CharRemoverBlock X",
                "charprocessor.UpperCaseBlock"
                ));

        BlockLoader blockLoader = new BlockLoader();
         List<Block> blocks = blockLoader.getBlocksFromNames(blockNames);
         ProcessingUnit processor = new ProcessingUnit(blocks);

         assertEquals(3, processor.blocks.size());
    }

    @Test
    public void testGetBlocksFromNameWithBadBlockNames(){
        List<String> blockNames = new ArrayList<>(Arrays.asList(
                "not.aValidBlock",
                "charprocessor.CharRemoverBlock X",
                "charprocessor.UpperCaseBlock"
                ));

        BlockLoader blockLoader = new BlockLoader();
         List<Block> blocks = blockLoader.getBlocksFromNames(blockNames);
         ProcessingUnit processor = new ProcessingUnit(blocks);

         assertEquals(2, processor.blocks.size());
    }
}
