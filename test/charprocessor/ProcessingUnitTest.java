package charprocessor;

import org.junit.runner.*;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.*;

import java.util.*;

import static org.junit.Assert.*;

import org.junit.Test;

@RunWith(Parameterized.class)
public class ProcessingUnitTest {
    ProcessingUnit processingUnit;
    List<Block> testBlocks;
    String testInput, testExpected;

    public ProcessingUnitTest(List<Block> blocks, String input, String expected) {
        testBlocks = blocks;
        testInput = input;
        testExpected = expected;
    }

    @Test
    public void canary() {
        assertTrue(true);
    }

    @Parameters
    public static Iterable<Object[]> parameterSetup() {
        List<Block> testBlocks1 = new ArrayList<>(),
                    testBlocks2 = new ArrayList<>(),
                    testBlocks3 = new ArrayList<>();

        testBlocks1.add(new UpperCaseBlock());
        testBlocks1.add(new CharRemoverBlock('Z'));
        testBlocks1.add(new LowerCaseBlock());

        testBlocks2.add(new UpperCaseBlock());
        testBlocks2.add(new LowerCaseBlock());

        testBlocks3.add(new CharRemoverBlock('f'));
        testBlocks3.add(new UpperCaseBlock());

        return Arrays.asList(new Object[][] {
            { testBlocks1, "11abcdabcdabcdzzaazzabcd", "11abcdabcdabcdaaabcd" },
            { testBlocks1, "aWsaAaAAAaaWaaA", "awsaaaaaaaawaaa" },
            { testBlocks2, "aaff33hh35v_552a", "aaff33hh35v_552a" },
            { testBlocks2, "aWsaAaAAAaaWaaA", "awsaaaaaaaawaaa" },
            { testBlocks3, "aaff33hh35v_552a", "AA33HH35V_552A" },
            { testBlocks3, "aWsaAaAAAaaWaaA", "AWSAAAAAAAAWAAA" } });
    }

    @Test
    public void testApplier(){
        processingUnit = new ProcessingUnit(testBlocks);
        assertEquals(testExpected, processingUnit.process(testInput));
    }
}
