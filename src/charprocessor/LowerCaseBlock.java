package charprocessor;

public class LowerCaseBlock implements Block {
    public String eval(char input) {
        return String.valueOf(Character.toLowerCase(input));
    }
}
