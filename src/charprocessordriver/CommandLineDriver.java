package charprocessordriver;

import charprocessor.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class CommandLineDriver {
    public static void main(String[] args) throws IOException{
        String inputCharacters = "";
        boolean nonInteractive = false;

        String processingUnitFileName = System.getProperty("user.dir") + "/" + "processing-unit.txt";
        System.out.println("Reading processing unit from: " + processingUnitFileName + ".\n\n");

        List<String> blocksNames = Files.readAllLines(Paths.get(processingUnitFileName));
        List<Block> blocks = new BlockLoader().getBlocksFromNames(blocksNames);
        ProcessingUnit processingUnit = new ProcessingUnit(blocks);

        do{
            System.out.print("Enter Input Characters: ");

            BufferedReader consoleReader = new BufferedReader(new InputStreamReader(System.in));
            inputCharacters = consoleReader.readLine();

            if(inputCharacters == null){
                inputCharacters = "11abcdabcdabcdzzaazzabcd";
                nonInteractive = true;
                System.out.println(inputCharacters);
            }

            if(inputCharacters.trim().equals("")) break;

            System.out.println("\nOutput Characters: " + processingUnit.process(inputCharacters) + "\n");
        }while(!nonInteractive);
    }
}
