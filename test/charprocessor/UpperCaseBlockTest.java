package charprocessor;

import org.junit.Test;
import org.junit.runners.Parameterized.*;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;

public class UpperCaseBlockTest extends BlockTest {
    public UpperCaseBlockTest(char input, String expected) {
        super(input, expected);
    }

    @Test
    public void canary() {
        assertTrue(true);
    }

    @Parameters
    public static Iterable<Object[]> parameterSetup() {
        return Arrays.asList(new Object[][] {
            { 'a', "A" },
            { 'z', "Z" },
            { 'Z', "Z" },
            { '1', "1" },
            { '&', "&" }
            });
    }

    @Override
    public Block createBlock() {
        return new UpperCaseBlock();
    }
}
