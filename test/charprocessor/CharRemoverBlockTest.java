package charprocessor;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.*;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;

@RunWith(Parameterized.class)
public class CharRemoverBlockTest {
    private CharRemoverBlock testBlock;
    private char testCharToRemove;
    private char testInput;
    private String testExpected;

    public CharRemoverBlockTest(char charToRemove, char input, String expected) {
        testCharToRemove = charToRemove;
        testInput = input;
        testExpected = expected;
    }

    @Before
    public void setUp() {
        testBlock = new CharRemoverBlock(testCharToRemove);
    }

    @Parameters
    public static Iterable<Object[]> parameterSetup() {
        return Arrays.asList(new Object[][] {{'z', 'a', "a"},
                                             {'z', 'z', "" },
                                             {'z', 'Z', "Z"},
                                             {'z', 'b', "b"},
                                             {'z', '8', "8"},
                                             {'k', 'k', "" }});
    }

    @Test
    public void testConverter() {
        assertEquals(testExpected, testBlock.eval(testInput));
    }
}
