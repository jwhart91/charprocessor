A program to showcase dynamic loading of components at runtime in Java.

This project is written in Java and uses Gradle to manage builds. Everything is
fully unit tested.
