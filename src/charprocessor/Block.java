package charprocessor;

public interface Block {
    public String eval(char input);
}
