package charprocessor;

public class MultiplierBlock implements Block {
    public String eval(char input) {
        return String.format("%s%s", input, input);
    }
}
