package charprocessor;

import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.joining;

public class ProcessingUnit {
    protected List<Block> blocks = new ArrayList<>();

    public ProcessingUnit(List<Block> inputBlocks) {
        blocks = inputBlocks;
    }

    public String process(String input){
        return blocks.stream()
                     .reduce(input, this::processABlock, String::concat);
    }

    private String processABlock(String input, Block block){
        return input.chars()
                    .mapToObj(letter -> (char)letter)
                    .map(block::eval)
                    .collect(joining(""));
    }
}
