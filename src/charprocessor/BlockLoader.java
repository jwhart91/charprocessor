package charprocessor;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

public class BlockLoader {
    public List<Block> getBlocksFromNames(List<String> blocksNames){
        List<Block> blocks = new ArrayList<>();

        for(String blockName : blocksNames){
            try {
                blocks.add(loadBlock(blockName));
            } catch (Exception e) {
                System.out.println("Error loading block: " + e.getMessage());
            }
        }

        return blocks;
    }

    public Block loadBlock(String className) throws ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        String[] blockNameWithArgs = className.split(" ");
        String constructorArg = blockNameWithArgs.length <= 1 ? null : blockNameWithArgs[1];

        if(constructorArg == null)
            return createInstanceWithoutArg(blockNameWithArgs[0]);
        else
            return createInstanceWithArg(blockNameWithArgs[0], constructorArg);
    }

    public Block createInstanceWithoutArg(String className) throws ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
        Class<?> klass = Class.forName(className);
        Constructor<?> constructor = klass.getConstructor();
        Object instance = constructor.newInstance();
        return (Block) instance;
    }

    public Block createInstanceWithArg(String className, String arg) throws ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
        Class<?> klass = Class.forName(className);
        Constructor<?> constructor = klass.getConstructor(char.class);
        Object instance = constructor.newInstance(arg.charAt(0));
        return (Block) instance;
    }
}
