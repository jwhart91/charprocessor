package charprocessor;

public class CharRemoverBlock implements Block {
    private final char toRemove;

    public CharRemoverBlock(char theChar) {
        toRemove = theChar;
    }

    public String eval(char input) {
        return input == toRemove ? "" : String.valueOf(input);
    }
}
