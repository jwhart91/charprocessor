package charprocessor;

import org.junit.Test;
import org.junit.runners.Parameterized.*;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;

public class LowerCaseBlockTest extends BlockTest {
    @Test
    public void canary() {
        assertTrue(true);
    }

    @Parameters
    public static Iterable<Object[]> parameterSetup() {
        return Arrays.asList(new Object[][] {{ 'a', "a" },
                                             { 'Z', "z" },
                                             { 'b', "b" },
                                             { '8', "8" },
                                             { '&', "&" } });
    }

    public LowerCaseBlockTest(char input, String expected) {
        super(input, expected);
    }

    @Override
    public Block createBlock() {
        return new LowerCaseBlock();
    }
}
